#!/usr/bin/env python3

# (C) 2013 Francois Cauwe
# (C) 2015-2018 Esben Haabendal

#Global libs
import sys
import time
import threading
import argparse
import socket
import subprocess

from ruamel.yaml import YAML
yaml=YAML(typ='safe')

#Private libs
from . import listen
from . import snmp

def main():
    parser = argparse.ArgumentParser(
        description='Brother network scanner server')
    parser.add_argument('-c', '--config', metavar='FILE',
                        type=str, default='brother-scan.yaml',
                        help='Configuration file')
    args = parser.parse_args()

    # Loading configuration
    try:
        with open(args.config) as configfile:
            config = yaml.load(configfile)
    except FileNotFoundError as e:
        print('Error: %s: %s'%(e.strerror, e.filename))
        sys.exit(1)

    config.setdefault('bind', {})
    config['bind'].setdefault('addr', '0.0.0.0')
    config['bind'].setdefault('port', 54925)
    config.setdefault('advertise', {})
    config['advertise'].setdefault('addr', config['bind']['addr'])
    config['advertise'].setdefault('port', config['bind']['port'])

    if 'scanner' not in config:
        sys.exit('Error: scanner not specified in configuration')
    if 'addr' not in config['scanner']:
        sys.exit('Error: scanner.addr not specified in configuration')
    if 'model' not in config['scanner']:
        sys.exit('Error: scanner.model not specified in configuration')
    config['scanner'].setdefault('name',
                                 f"Brother-{config['scanner']['model']}")

    # Resolv hosts
    for key in ('bind', 'advertise', 'scanner'):
        config[key]['addr'] = socket.gethostbyname(config[key]['addr'])

    # Configure scanner (if needed)
    #sane.init()
    #scanners = [ s for s in sane.get_devices() if s[0].startswith('brother4:net') ]
    #if not scanners:
    cmd = ['brsaneconfig4', '-a',
           f"name={config['scanner']['name']}",
           f"model={config['scanner']['model']}",
           f"ip={config['scanner']['addr']}"]
    print(f"Configuring scanner: {' '.join(cmd)}")
    subprocess.call(cmd)

    # Start Snmp
    listenThread = threading.Thread(target=listen.launch, args=(config,))
    listenThread.start()
    time.sleep(1)

    # Start Snmp
    snmpThread = threading.Thread(target=snmp.launch, args=(config,))
    snmpThread.start()

    # Wait for closing
    snmpThread.join()
    listenThread.join()

if __name__ == '__main__':
    main()
