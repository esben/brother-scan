import sys
import socket
import sane

from .scanto import scanto

def launch(config):

    addr = (config['bind']['addr'], config['bind']['port'])
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(addr)
    print("Listening on %s:%d"%(addr))

    last_seq = None
    while(1):
        data, addr = server_socket.recvfrom(4096)
        print("Got UDP packet: %d bytes from %s:%s"%(
            len(data), addr[0], addr[1]))
        if len(data) < 4 or data[0] != 2 or data[1] != 0 or data[3] != 0x30:
            print('Error: dropping unknown UDP data: %d bytes'%(len(data)))
            continue
        msg = data[4:].decode('utf-8')
        print('Received:', msg)
        msgd = {}
        for item in msg.split(';'):
            if not item:
                continue
            name, value = item.split('=')
            if name == 'USER':
                value = value[1:-1]
            if name == 'SEQ':
                seq = int(value)
            msgd[name] = value
        if seq == last_seq:
            print('Skipping duplicate scan request')
            continue
        last_seq = seq

        sane.init()
        scanners = [ s for s in sane.get_devices() if s[0].startswith('brother4:net') ]
        if not scanners:
            sys.exit("Error: Scanner not found")
        print(f"Opening scanner: {scanners[0][0]}")
        scanner = sane.open(scanners[0][0])

        for menu_func, menu_users in config['menu'].items():
            for menu_user, menu_entry in menu_users.items():
                menu_func = menu_func.upper()
                if msgd['FUNC'] == menu_func and msgd['USER'] == menu_user:
                    print(f'scanto {menu_func} {menu_user} {menu_entry}')
                    scanto(scanner, menu_entry)
                    break
