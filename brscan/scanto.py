import sys
import os
import datetime
import sane

# Patch python-sane v2.8.3 to allow use with Python 3
if '__next__' not in dir(sane._SaneIterator):
    sane._SaneIterator.__next__ = sane._SaneIterator.next

def scanto(scanner, options):
    # Apply options
    for key, value in options.items():
        if key in ('dir',):
            continue
        if key not in scanner.optlist:
            print(f"Warning: invalid option {key}")
            continue
        try:
            setattr(scanner, key, value)
        except TypeError as e:
            print(f"Warning: invalid {key} value: {value}: {e}")
            continue

    # Scan all pages (ADF or multiple pages on flatbed)
    pages = []
    for page in scanner.multi_scan():
        pages.append(page)
    if not pages:
        sys.exit("Error: no pages scanned")
    print(f'Scanned {len(pages)} pages')

    # Create output directory
    output_dir = options.get('dir', '/tmp')
    os.makedirs(output_dir, exist_ok=True)

    # Write PDF file
    now = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    output_file = f'{output_dir}/scan_{now}.pdf'
    first_page = pages.pop(0)
    resolution = options.get('resolution', 150)
    first_page.save(output_file, save_all=True, append_images=pages,
                    resolution=resolution)
    print('Wrote', output_file)
