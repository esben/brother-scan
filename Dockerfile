FROM archlinux:base-devel
MAINTAINER Esben Haabendal, esben@haabendal.dk

# brscan4 depends
RUN pacman -Sy --noconfirm sane libusb-compat gtk2

# Building brscan4 package from AUR
RUN pacman -Sy --noconfirm git
RUN useradd user -m

RUN su user -c "cd /home/user \
 && git clone https://aur.archlinux.org/brscan4.git \
 && cd brscan4 \
 && makepkg"
# Install it as a crude smoke-test

# Build the brother-scan Python package
RUN pacman -Sy --noconfirm python python-pip python-wheel
RUN su user -c "mkdir /home/user/brother-scan"
RUN pacman -Sy --noconfirm imagemagick ghostscript
COPY requirements.txt /home/user/brother-scan
RUN pip install -r /home/user/brother-scan/requirements.txt
COPY brscan/ /home/user/brother-scan/brscan/
COPY setup.py README.md /home/user/brother-scan/
RUN su user -c "cd /home/user/brother-scan \
 && python setup.py sdist bdist_wheel"


FROM archlinux:base
MAINTAINER Esben Haabendal, esben@haabendal.dk

# This is where the scan output will be written to
VOLUME /output

# This must be mapped to ${ADVERTISE_IP}:54925
EXPOSE 54925/udp

# Install brscan4 dependencies
RUN pacman -Sy --noconfirm sane libusb-compat gtk2

# Install brscan4 package from previous stage
COPY --from=0 /home/user/brscan4/brscan4-*.tar.zst /tmp
RUN pacman -U --noconfirm /tmp/brscan4-*.tar.zst

# Install brother-scan Python and required modules
RUN pacman -Sy --noconfirm python python-pip python-wheel \
      python-sane python-pillow python-pysnmp
COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt
COPY --from=0 /home/user/brother-scan/dist/*.whl /tmp
RUN pip install /tmp/*.whl

# Cleanup (only makes sense when used together with --squash/--squash-all)
RUN rm -rf /tmp/* && yes | pacman -Scc --noconfirm && rm -rf /var/cache/pacman/*

VOLUME /brother-scan.yaml
CMD brscand -c /brother-scan.yaml
